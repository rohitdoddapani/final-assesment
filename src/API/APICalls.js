import A1 from '../assets/songs/A1.mp3';
import A2 from '../assets/songs/A2.mp3';
import A3 from '../assets/songs/A3.mp3';

export default{
    getSongs: () => {
        var songs = [
          {id:1,title:'Ninnila',artists:'Armaan Malik',audio: A1,poster:'https://a10.gaanacdn.com/gn_img/albums/MmqK5EKwRO/mqK5vpy13w/size_xxl_1516425678.webp',duration:'3:54'},
          {id:2,title:'Nee Kallalona',artists:'Vedala Hemachandra',audio:A2,poster:'https://i1.wp.com/lyricbasket.in/wp-content/uploads/2017/09/jr-ntr-nivetha-thomas-in-jai-lava-kusa-movie-song-nee-kallalona-posters.jpg?fit=1200%2C1200&ssl=1',duration:'3:28'},
          {id:3,title:'Jwala Reddy',artists:'Mangli',audio:A3,poster:'https://atozmp3.cc/wp-content/uploads/2021/03/Seetimaar-2021-Jwala-Reddy-FM.jpg',duration:'4:04'},
          {id:4,title:'Saranga Dariya',artists:'Mangli',audio:A3,poster:'http://a10.gaanacdn.com/images/song/37/34988737/crop_480x480_1614488967.jpg',duration:'4:20'},
        ]
      return songs;
  },
}