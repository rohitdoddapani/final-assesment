import Vue from 'vue'
import Vuex from 'vuex'
import A1 from '../assets/songs/A1.mp3';
import A2 from '../assets/songs/A2.mp3';
import A3 from '../assets/songs/A3.mp3';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    search: '',
    currentSong: {id:1,title:'Ninnila',artists:'Armaan Malik',audio:A1,poster:'https://a10.gaanacdn.com/gn_img/albums/MmqK5EKwRO/mqK5vpy13w/size_xxl_1516425678.webp',duration:'3:54'},
    currentQueue: [
        {id:1,title:'Ninnila',artists:'Armaan Malik',audio:A1,poster:'https://a10.gaanacdn.com/gn_img/albums/MmqK5EKwRO/mqK5vpy13w/size_xxl_1516425678.webp',duration:'3:54'},
        {id:2,title:'Nee Kallalona',artists:'Vedala Hemachandra',audio:A2,poster:'https://i1.wp.com/lyricbasket.in/wp-content/uploads/2017/09/jr-ntr-nivetha-thomas-in-jai-lava-kusa-movie-song-nee-kallalona-posters.jpg?fit=1200%2C1200&ssl=1',duration:'3:28'},
    ],

  },
  getters: {
    getCurrentSong(state){
      return state.currentSong;
    },
    getCurrentQueue(state){
      return state.currentQueue;
    }
  },
  mutations: {
    setCurrentSong(state,payload){
      state.currentSong = payload;
    },
    setCurrentQueue(state,payload){
      state.currentQueue.unshift(payload);
    },
    shuffleCurrentQueue(state){
      state.currentQueue.sort( () => Math.random() - 0.5);
    },
    playPrevSong(state,payload){
      var index = state.currentQueue.indexOf(payload);
      if(index==0){
        state.currentSong = state.currentQueue.slice(-1)[0]
      }else{
        state.currentSong = state.currentQueue[index-1]
      }
    },
    playNextSong(state,payload){
      var index = state.currentQueue.indexOf(payload);
      if(index==state.currentQueue.length-1){
        state.currentSong = state.currentQueue[0]
      }else{
        state.currentSong = state.currentQueue[index+1]
      }
    }
  },
  actions: {
  },
  modules: {
  }
})
